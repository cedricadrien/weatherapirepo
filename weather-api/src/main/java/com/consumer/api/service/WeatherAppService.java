package com.consumer.api.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consumer.api.client.WeatherClient;
import com.consumer.api.dao.WeatherInfoDao;
import com.consumer.api.model.WeatherInfo;
import com.consumer.api.model.WeatherLog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class WeatherAppService {

	@Autowired
	WeatherClient client;
	
	@Autowired
	WeatherInfoDao weatherInfoDao;
	
	public String getWeatherInfo(Long locationId) {
		WeatherInfo weatherInfo = client.getWeatherInfo(locationId);
		ObjectMapper mapper = new ObjectMapper();
		String weatherInfoJson = "";
		try {
			weatherInfoJson = mapper.writeValueAsString(weatherInfo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return weatherInfoJson; 
	}
	
	public List<WeatherLog> getRecents() {
		return weatherInfoDao.findFirst5ByOrderByIdDesc();
	}
	
	public void saveWeatherInfo(WeatherInfo weatherInfo) {
		WeatherLog log = new WeatherLog();
		log.setLocation(weatherInfo.getLocation());
		StringBuilder actualWeather = new StringBuilder();
		weatherInfo.getWeather().forEach(w -> actualWeather.append(w.getActualWeather() +","));
		actualWeather.deleteCharAt(actualWeather.length()-1);
		log.setActualWeather(actualWeather.toString());
		log.setTemperature(weatherInfo.getTemp().getCurrentTemp().toString());
		weatherInfoDao.save(log);
	}
}
