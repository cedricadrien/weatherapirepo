package com.consumer.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Temperature {

	@JsonProperty("temp")
	private Long currentTemp;

	public Long getCurrentTemp() {
		return currentTemp;
	}

	public void setCurrentTemp(Long currentTemp) {
		this.currentTemp = currentTemp;
	}

	@Override
	public String toString() {
		return "Temperature [currentTemp=" + currentTemp + "]";
	}
	
	
}
