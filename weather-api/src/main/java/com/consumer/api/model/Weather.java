package com.consumer.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Weather {

	@JsonProperty("description")
	private String actualWeather;

	public String getActualWeather() {
		return actualWeather;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}

	@Override
	public String toString() {
		return "Weather [actualWeather=" + actualWeather + "]";
	}
	
	
}
