package com.consumer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.consumer.api.model.WeatherInfo;
import com.consumer.api.model.WeatherLog;
import com.consumer.api.service.WeatherAppService;

@RestController
public class WeatherAppController {

	@Autowired
	private WeatherAppService weatherAppService;
	
	
	@GetMapping(value="/getWeatherInfo/{locationId}")
	public String getWeatherInfo(@PathVariable Long locationId) {
		return weatherAppService.getWeatherInfo(locationId);
	}
	
	@PostMapping(value="/saveWeatherInfo")
	public void saveWeatherInfo(@RequestBody WeatherInfo weatherInfo) {
		weatherAppService.saveWeatherInfo(weatherInfo);
	}
	
	@GetMapping("/getRecents")
	public List<WeatherLog> getRecents() {
		return weatherAppService.getRecents();
	}
}