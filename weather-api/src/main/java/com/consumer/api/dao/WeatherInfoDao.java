package com.consumer.api.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.consumer.api.model.WeatherLog;

public interface WeatherInfoDao extends CrudRepository<WeatherLog,Long> {

	public List<WeatherLog> findFirst5ByOrderByIdDesc();
}
